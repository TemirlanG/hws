one = ['varta', 'duracell', 'energizer']

def one1(*args):
    x = ['battery']
    for y in args:
        x.append(y[0])
        x.append(y[1])
        x.append(y[2])
    return x
z = one1(one)
print(z)



def two(equipment, **kwargs):
    xy = {
        'equipment': equipment,
        'equipment_id': kwargs['equipment_id']+1
    }
    return xy

xyz = two('Laptop', equipment_id=1000)
print(xyz)


def three(*args, **kwargs):
    three1 = {
        'resolution': args,
        'id':kwargs['id']+10
    }
    return three1

three2 = three(1920, 1080, id=1000)
print(three2)


def four(brand, model, *args, **kwargs):
    four1 = {
        'brand': brand,
        'model': model,
        'resolution': args,
        'id': kwargs['id']+1
        }
    return four1

four2 = four('AOC', 'super 4K', 3840, 2160, id=100)
print(four2)


five = lambda int: int * 5
five1 = five(2048)
print(five1)

six = lambda int: int / 5
six1 = six(1024)
print(six1)

seven = {
    'first_name': 'Boris',
    'last_name': 'Johnson',
    'age': 56
    }

seven1 = lambda age: print(age['age'])
seven1(seven)


eight = ['example']
print(eight)
eight1 = 'Python'
eight2 = list(eight1)
print(eight1)

nine = [1, 2, 3, 4, 5]
print(nine[0:3])

nine[0] = False
print(nine)

nine.append(6)
print(nine)

nine.remove(6)
print(nine)

therteen = tuple(nine)
print(therteen)

fourteen = ('+', '-', '=')
fourteen1 = tuple('Hello, world!')
print(fourteen)
print(fourteen1)

fifteen = ('ABCD', 'АБВГД')
print(fifteen[0])

sixteen = list(fifteen)
print(sixteen)

seventeen = {
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5
}
seventeen1 = dict(one = 1, two = 2, three=3)
print(seventeen1)

print(seventeen['one'])

seventeen['one'] = True
print(seventeen)

seventeen['six'] = 6
print(seventeen)

seventeen.pop('two')
print(seventeen)

print(one)
one.sort(key = lambda one: one)
print(one)


twentythree = ['samsung', 'apple', 'xiaomi', 'huawei']
print(twentythree)
twentythree1 = lambda list: print(sorted(list))
twentythree1(twentythree)


twentyfour = {'Jet', 'Brains', 'super', 'Jet'}
print(twentyfour)

twentyfour1 = set('bill windows')
print(twentyfour1)