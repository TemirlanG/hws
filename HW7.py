one = frozenset('qwerty')
one1 = ('q', 'w', 'e', 'r', 't', 'y')
one2 = frozenset(one1)
print(one)
print(one2)

two = {
    'processor': 'intel',
     'video': 'Nvidia',
     'motherboard': 'Asus'
    }

two1 = frozenset(two)
print(two1)

three = {'A', 'B', 'C', 'D'}
print(three)
three.add('E')
print(three)

three1 = ('a', 'b', 'c')
three.add(three1)
print(three)

three.clear()
print(three)

four = {'one', 'two', 'three', 'four', 'five'}
four1 = four
print(four1)
four2 = four.copy()
print(four2)

five = {3, 3.1, 3.2, 3.3, True}
five2 = {3, 3.1, 3.2, 3.9, False}
print(five.difference(five2))
print(five2.difference(five))
print(five - five2)
print(five2 - five)

five3 = five.difference_update(five2)
print('difference_update five', five)
print('difference_update five2', five2)

five.discard(True)
print('discard', five)

six = {100, 200, 300, 400}
six1 = {200, 300, 100, 500}
six2 = {500, 400, 600, 100}
print('intersection', six.intersection(six1, six2))

seven = six.intersection_update(six1, six2)
print('intersection_update six', six)

eight = {500, 600, 700, 800}
print('isdisjoint six1 -', six.isdisjoint(six1))
print('isdisjoint eight -',six.isdisjoint(eight))


nine = {800, 700, 600}
print('issubset nine -', nine.issubset(eight))
print('issubset six2 -', nine.issubset(six2))

print('pop', eight.pop())

print(four2)
four2.remove('four')
print('removed four', four2)

print(six1.symmetric_difference(six2))
print(six1 ^ six2)

ten = six1.symmetric_difference_update(six2)
print('symmetric_difference_update six1', six1)
print('symmetric_difference_update six2', six2)

print('union', four2.union(nine, five2))
print('union |', five2 | eight | five)

eleven = {'u', 'i', 'o'}
eleven.update(one)
print(eleven)
eleven1 = 'a'
eleven.update(eleven1)
print(eleven)



loading = list(range(0, 101))
for load in loading:
    if load != 100:
        print('loading', load, '%')
        continue
    print('finished', load, '%')


for load in loading:
        if load == 10:
            break
        print('loading', load, '%')
print('error on', load, '%')