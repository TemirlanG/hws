zero = 0
one = 1
two = 2
three = 3
four = 4
five = 5
six = 6
seven = 7
eight = 8

print(zero > one)
print(two > one)

print(three < one)
print(two < three)

print(six == zero)

print(two >= two)
print(one >= six)

print(four <= six)
print(seven <= five)

print(five < six < seven)
print(eight < five < four < seven)

print(eight > five > four > two)
print(zero > four > eight)

var = 51
var1 = 106
var2 = 210
var3 = 25

if var < var1:
    print('right')

if var > var1:
    print('right')
else:
    print('wrong')


if var > var1:
    print('more')
elif var2 < var3:
    print('less')
elif var3 == var:
    print('equal')
else:
    print('ЛОЖЬ')

list0 = [24, 9.0, 'char', True]
print(list0)

list0.pop(2)
print(list0)

list0.pop(2)
list0.append(False)
print(list0)

dict0 = {'integer': 543,
         'float': 89.2,
         'char': 'letter',
         'boolean': False}

print(dict0)

dict0.pop('float')
print(dict0)

dict0 ['integer'] = '99'
print(dict0)


list1 = ['corsair', 'MSI', 'gigabyte', 'steelseries', 'HyperX']
list1.append('EVGA')
print(list1)

list1.clear()
print(list1)

list2 = ['corsair', 'MSI', 'gigabyte', 'steelseries', 'HyperX', 'ZOWIE']
list2.reverse()
print(list2)

dict1 = {'Mercedes': 'Hamilton',
         'Red Bull': 'Perez',
         'McLaren': 'Ricciardo',
         'Aston Martin': 'Vettel'}
dict1.clear()
print(dict1)

dict2 = {'Mercedes': 'Hamilton',
         'Red Bull': 'Perez',
         'McLaren': 'Ricciardo',
         'Aston Martin': 'Vettel',
         'Alpine': 'Alonso'}
print(dict2.get('Alpine'))


string0 = 'moloko, yaica, muka, kartoshka, maslo, myaso, hleb, poroshok'
print(string0)
print(string0.find('maslo'))

print(string0.split(','))