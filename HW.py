#1
one = 'FN '
two = 'Hestral '
three = 'Fabrique '
four = 'Nationale'
result = one + two + three + four
print(result)

#2
one1 = f'FN {three}Nationale'
print(one1)

#3
two2 = 'FN {}Nationale {}'.format(three, two)
print(two2)


first = 7
second = 2
third = 9

#4
res4 = first + second
print(res4)

#5
res5 = first - second
print(res5)

#6
print(first * second)

#6
print(first / second)

#7
res7 = first ** second
print(res7)

#8
res8 = third % second
print(res8)

#9
res9 = (third * second) / first
print(res9)

#10
res10 = 11.2 * 2.3
print(res10)

#11
res11 = two.upper()
print(res11)

#12
res12 = one.lower()
print(res12)

#13
res13_1 = 'drinkwater'
res13_2 = res13_1.capitalize()
print(res13_2)

