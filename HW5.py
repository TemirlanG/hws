def func0():
    print('values')


func0()


def func1(*args):
    AG = 1
    for nums in args:
        AG *= nums
    return AG


print(func1(1000, 200, 300))

list0 = [['just', 'because'], ['you', 'know']]

for words in list0:
    for letters in words:
        print(letters)

club_users = [
    {
        'name': 'Sultan',
        'gender': 'M',
        'age': 27
    },
    {
        'name': 'Nurik',
        'gender': 'M',
        'age': 28
    },
    {
        'name': 'Vlad',
        'gender': 'M',
        'age': 20
    },
    {
        'name': 'Meerim',
        'gender': 'F',
        'age': 24
    },
    {
        'name': 'Ayka',
        'gender': 'F',
        'age': 17
    },
    {
        'name': 'Lera',
        'gender': 'F',
        'age': 20
    },
    {
        'name': 'Ruslan',
        'gender': 'M',
        'age': 19
    }
]

my_friend = [
    {
        'name': 'Aizana',
        'gender': 'female',
        'age': 17
    },
    {
        'name': 'Ajara',
        'gender': 'female',
        'age': 22
    },
    {
        'name': 'Scarlett',
        'gender': 'female',
        'age': 23
    }
]


def security(*args):
    passed = []

    for clubers in args:
        for u_club in clubers:
            if u_club['age'] >= 18:
                passed.append(u_club)
                u_club['status'] = True
    return passed

def administrator(clients):
    for client in clients:
        print(client)
        client['status'] = True


result = security(my_friend, club_users)
print(result)

admin = administrator(result)
print(admin)